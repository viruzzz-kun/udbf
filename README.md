uDBF - Library for reading and writing DBF files
================================================

Example usage
-------------

    from udbf import StreamingDBF
    
    with open('myfile.dbf', 'rb') as dbf_stream:
        dbf = StreamingDBF(dbf_stream)
        
        for field in dbf.fields:
            print(field.name)
        print()
        
        for record in dbf:
            print record.as_dict()


Writing
-------

Writing is in development, as well as creating.

Pull requests
-------------

...are praised
