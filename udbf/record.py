import six
from attr import attrs, attrib, Factory, validators


@attrs(slots=True)
class RecordBase(object):
    data = attrib(default=Factory(tuple))
    deleted = attrib(default=False, validator=validators.instance_of(bool))
    dirty = attrib(default=True, validator=validators.instance_of(bool))

    structure = None  # type: struct.Struct
    _dbf = None  # type: DBF
    fields = property(lambda self: self._dbf.header.fields)
    fields_index_map = property(lambda self: self._dbf.header.fields_index_map)

    def __getitem__(self, key):
        data_index, field = self._get_data_index(key)
        return field.to_py(self.data[data_index])

    def __setitem__(self, key, value):
        data_index = self._get_data_index(key)
        field = self.fields[key]
        value = field.from_py(value)
        self.data = tuple(
            v if i != data_index else value
            for i, v in enumerate(self.data)
        )
        self.dirty = True

    def as_dict(self):
        return {
            field.name: field.to_py(value)
            for field, value in six.moves.zip(self.fields, self.data)
        }

    def update(self, data):
        clean_data = {
            six.text_type(key): self.fields[six.text_type(key)].from_py(value)
            for key, value in six.iteritems(data)
        }
        self.data = tuple(
            clean_data.get(field.name, orig_data)
            for field, orig_data in six.moves.zip(self.fields, self.data)
        )

    def _get_data_index(self, key):
        if isinstance(key, six.integer_types):
            index, field = key, self.fields[key]
            return index, field
        if isinstance(key, six.binary_type):
            key = six.text_type(key)
        if isinstance(key, six.text_type):
            index = self.fields_index_map[key]
            field = self.fields[index]
            return index, field
        raise ValueError('{0!r} is not a valid field name or index'. format(key))

    @classmethod
    def from_db(cls, db_record):
        values = cls.structure.unpack(db_record)
        fields = cls._dbf.header.fields
        deleted, values = values[0] == b'*', values[1:]
        assert len(values) == len(fields)

        data = tuple(
            field.from_db(value) for field, value in six.moves.zip(fields, values)
        )
        return cls(data=data, deleted=deleted, dirty=False)

    def to_db(self):
        parts = [b'*' if self.deleted else b' ']
        parts.extend(
            self._dbf[field_name].to_db(value)
            for field_name, value in six.iteritems(self.data)
        )
        return self.structure.pack(parts)
