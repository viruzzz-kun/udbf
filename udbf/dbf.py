import io

import datetime
import os

import six
from attr import validators, attrib, attrs, Factory

from udbf.header import StandardDBFHeader
from udbf.memo import FoxProMemoFile, DBase4MemoFile, DBase3MemoFile
from udbf.memo import MemoFile


class RecordsCache(dict):
    _dbf = None

    def list_dirty_records(self):
        dirty_records = [
            (rec_no, record)
            for rec_no, record in six.iteritems(self)
        ]
        dirty_records.sort()
        return dirty_records

    @classmethod
    def for_dbf(cls, dbf):
        # noinspection PyCallingNonCallable
        self = cls()
        self._dbf = dbf


@attrs()
class DBF(object):
    stream = attrib()  # type: io.FileIO
    memo_stream = attrib(default=None)  # type: io.FileIO

    name = attrib(default=Factory(
        lambda self: getattr(self.stream, 'name', u''),
        takes_self=True
    ))  # type: six.string_types
    header = attrib(default=Factory(
        lambda self: StandardDBFHeader.from_stream(self.stream, self),
        takes_self=True
    ))  # type: StandardDBFHeader
    read_only = attrib(default=True)
    charset = attrib(default='utf8')
    is_new = attrib(default=False)
    is_dirty = attrib(default=False)
    memo_file = attrib(default=Factory(
        lambda self: self._initialize_memo_file(),
        takes_self=True
    ))  # type: MemoFile

    # noinspection PyArgumentList
    def _initialize_memo_file(self):
        memo_class = None  # type: MemoFile
        if self.header.type_code in (0x30, 0x31, 0x32, 0xf5,):
            memo_class = FoxProMemoFile
        elif self.header.type_code in (0x8b, 0xcb,):
            memo_class = DBase4MemoFile
        elif self.header.type_code in (0x83, 0xfb,):
            memo_class = DBase3MemoFile

        if memo_class and self.memo_stream:
            return memo_class.init(stream=self.memo_stream, name=self.name, is_new=self.is_new)
        memo_file_required = any(field.code == b'M' for field in self.header.fields)
        if memo_file_required:
            if not self.memo_stream:
                raise RuntimeError('Memo file is required, but no stream provided')
            elif not memo_class:
                raise RuntimeError('Memo file is required, but cannot determine class for type {!r}'.format(self.header.type_code))

    def close(self):
        self.stream.close()

    def __len__(self):
        return self.header.record_count

    closed = property(lambda self: self.stream.closed)
    fields = property(lambda self: self.header.fields)


@attrs(init=False)
class DBFFile(DBF):
    records_cache = attrib(
        default=Factory(RecordsCache.for_dbf, takes_self=True),
        validator=validators.instance_of(RecordsCache),
    )

    def _ensure_record(self, record):
        result = record

        if record is None:
            result = self.header.record_class()

        elif not isinstance(record, self.header.record_class):
            result = self.header.record_class()
            result['record'] = record

        return result

    def append_record(self, record=None, **kwargs):
        record = self._ensure_record(record)
        record.update(kwargs)

        rec_no = self.header.record_count
        self.header.record_count += 1
        self.records_cache[rec_no] = record
        self.is_dirty = True
        self.is_new = False

    def append_field(self, field):
        if self.is_dirty:
            raise RuntimeError('Cannot reshape dirty database')
        self.header.add_field(field)

    def flush(self):
        if self.is_dirty:
            self.header.last_update = datetime.date.today()
            self.stream.seek(0)
            self.stream.write(self.header.to_structure())

            for rec_no, record in self.records_cache.list_dirty_records():
                self._store_record(rec_no, record)
            self.records_cache.clear()

            self.stream.flush()

            if self.memo_file:
                self.memo_file.flush()

    def _load_record(self, rec_no):
        # rec_no should be absolute at this point!
        # rec_no = self._check_index(rec_no)
        self.stream.seek(self.header.record_start + rec_no * self.header.record_length)
        data = self.stream.read(self.header.record_length)
        record = self.header.record_class.from_db(data)
        return record

    def _store_record(self, rec_no, record=None):
        # rec_no should be absolute at this point!
        # rec_no = self._check_index(rec_no)
        if record is None:
            record = self.records_cache.get(rec_no)
        if record is None:
            raise RuntimeError('Record no {0} not found'.format(rec_no))
        self.stream.seek(self.header.record_start + rec_no * self.header.record_length)
        self.stream.write(self.header.record_class.to_db(record))

    def _check_index(self, rec_no):
        assert isinstance(rec_no, six.integer_types)
        if rec_no < 0:
            rec_no += len(self)
        if rec_no > len(self):
            raise IndexError('Record number out of range')
        return rec_no

    def __getitem__(self, rec_no):
        if isinstance(rec_no, slice):
            return [self[rec_no] for rec_no in six.moves.range(rec_no.start, rec_no.stop, rec_no.step)]

        if rec_no in self.records_cache:
            return self.records_cache[rec_no]
        else:
            result = self._load_record(self._check_index(rec_no))
            self.records_cache[rec_no] = result
            return result

    def __setitem__(self, rec_no, record):
        self.is_dirty = True
        self.is_new = False
        rec_no = self._check_index(rec_no)
        record = self._ensure_record(record)
        self.records_cache[rec_no] = record

    def __iter__(self):
        for rec_no in six.moves.range(self.header.record_count):
            yield self[rec_no]


class StreamingDBF(DBF):
    def __iter__(self):
        self.stream.seek(self.header.record_start)
        for _ in six.moves.range(self.header.record_count):
            data = self.stream.read(self.header.record_length)
            record = self.header.record_class.from_db(data)
            yield record


# noinspection PyArgumentList
def open_file(filename, read_only=False, charset=None):
    is_new = False
    if read_only:
        file_mode = 'rb'
    else:
        if not os.path.exists(filename):
            is_new = True
            file_mode = 'w+b'
        else:
            file_mode = 'r+b'
    stream = io.open(filename, file_mode)
    return DBFFile(
        stream, filename,
        charset=charset,
        read_only=read_only,
        is_new=is_new,
    )


# noinspection PyArgumentList
def create_file(filename, header, charset=None):
    stream = io.open(filename, 'w+b')
    return DBFFile(
        stream, filename,
        charset=charset,
        is_new=True,
    )


# noinspection PyArgumentList
def open_stream(stream, read_only=True, charset=None):
    return StreamingDBF(
        stream, getattr(stream, 'name', '<stream>'),
        charset=charset,
        read_only=read_only,
        is_new=False,
    )
