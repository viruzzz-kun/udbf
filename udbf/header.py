import collections
import datetime
import enum
import struct

import six
from attr import attrs, attrib, validators, Factory

from udbf.field import FieldBase
from udbf.record import RecordBase


class DBFHeader(object):
    __header_type_registry = {}

    @classmethod
    def get_class_for_type(cls, typecode):
        return cls.__header_type_registry[typecode]

    def __subclasshook__(cls, subclass):
        if not hasattr(subclass, 'typecode'):
            return
        if isinstance(subclass.typecode, six.integer_types):
            cls.__header_type_registry[subclass.typecode] = subclass
        elif isinstance(subclass.typecode, (list, tuple)):
            for typecode in subclass.typecode:
                cls.__header_type_registry[typecode] = subclass
        else:
            raise UserWarning(u'{0}.typename should be either int or list/tuple of ints')


@enum.unique
class TypeCodeBits(enum.Enum):
    DBASE_WINDOWS_VERSION = 0x7
    DBASE_5 = 0x08
    DBASE_7 = 0x10
    DBASE_MEMO = 0x88
    DBASE_SQL = 0x70
    DBASE_DBT_MEMO = 0x80


@attrs(slots=True)
class StandardDBFHeader(object):
    type_code = attrib(default=0, validator=validators.instance_of(int))
    last_update = attrib(default=Factory(datetime.date.today), validator=validators.instance_of(datetime.date))
    record_count = attrib(default=0, validator=validators.instance_of(int))
    record_start = attrib(default=0, validator=validators.instance_of(int))
    record_length = attrib(default=0, validator=validators.instance_of(int))
    has_cdx = attrib(default=False, validator=validators.instance_of(bool))
    has_memo = attrib(default=False, validator=validators.instance_of(bool))
    is_dbc = attrib(default=False, validator=validators.instance_of(bool))
    code_page_mark = attrib(default=0, validator=validators.instance_of(int))

    fields = attrib(factory=list)
    fields_index_map = attrib(factory=collections.OrderedDict, validator=validators.instance_of(collections.OrderedDict))

    record_class = attrib(default=None, validator=validators.instance_of((RecordBase, type(None))))
    _dbf = attrib(default=None)

    structure = struct.Struct('<BBBBIHH16xBBxx')

    @classmethod
    def from_stream(cls, stream, dbf=None):
        """
        :type stream: io.StringIO | cStringIO.StringIO
        :type dbf: udbf.dbf.DBF
        :param stream:
        :return:
        """
        stream.seek(0)
        data = stream.read(32)
        type_code, year, month, day, record_count, record_start, record_length, flags, code_page_mark = cls.structure.unpack(data)
        year = year + 2000 if year < 80 else year + 1900
        rv = cls(
            type_code, datetime.date(year, month, day), record_count, record_start, record_length,
            bool(flags & 0x01),  # has cdx file
            bool(flags & 0x02),  # has memo file
            bool(flags & 0x04),  # is dbc file
            code_page_mark,
        )
        rv._dbf = dbf

        field_index = 0

        data = stream.read(1)
        while data != b'\x0d':
            data += stream.read(31)
            if len(data) != 32:
                raise EOFError('Unexpected end of file')
            field = FieldBase.from_structure(data, dbf)  # type: FieldBase
            if field.name in rv.fields:
                raise ValueError('Duplicate field [{0}]'.format(field.name))
            rv.fields.append(field)
            rv.fields_index_map[field.name] = field_index
            data = stream.read(1)
            field_index += 1

        @attrs(slots=True)
        class Record(RecordBase):
            structure = rv.get_record_struct()
            _dbf = dbf

        rv.record_class = Record
        return rv

    def _calc_header_length(self):
        return self.structure.size + len(self.fields) * FieldBase.structure.size

    def to_structure(self):
        self.record_start = self._calc_header_length()
        # noinspection PyListCreation
        parts = []
        parts.append(self.structure.pack(
            self.type_code, self.last_update.year - 2000, self.last_update.month, self.last_update.day,
            self.record_count, self.record_start, self.record_length,
            (int(self.has_cdx) | int(self.has_memo) << 1 | int(self.is_dbc) << 2),
            self.code_page_mark,
        ))
        parts.extend([
            field.to_structure()
            for field in self.fields  # type: FieldBase
        ])
        parts.append(b'\x0d')
        return self.structure.pack(parts)

    def get_record_struct(self):
        struct_string = '<c' + ''.join(field.struct_template for field in self.fields)
        return struct.Struct(struct_string)

    def add_field(self, field):
        next_index = len(self.fields)
        self.fields.append(next_index)
        self.fields_index_map[field.name] = next_index
        self.record_start = len(self.fields) * FieldBase.structure.size

    def __getitem__(self, item):
        if isinstance(item, six.integer_types):
            return self.fields[item]
        index = self.fields_index_map[item]
        return self.fields[index]
