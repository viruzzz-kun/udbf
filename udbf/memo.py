import math
import struct
from enum import unique, Enum

import six
from attr import attrs, attrib, validators


@attrs()
class MemoFile(object):
    name = attrib()  # type: six.text_type
    stream = attrib()  # type: six.moves.StringIO

    block_size = attrib(default=512, validator=validators.instance_of(int))
    block_count = attrib(default=1, validator=validators.instance_of(int))
    is_new = attrib(default=False, validator=validators.instance_of(bool))

    extra_bytes = 0

    _header_struct = struct.Struct('>LxxH')

    def read_header(self):
        self.stream.seek(0)
        data = self.stream.read(8)
        self.block_count, self.block_size = self._header_struct.unpack(data)
        return self

    def write_header(self):
        self.stream.seek(0)
        self.stream.write(self._header_struct.pack(self.block_count, self.block_size))  # 8 bytes
        self.stream.write(b'\x00' * 8 + b'\x03' + b'\x00' * 495)

    @classmethod
    def init(cls, stream, name=None, is_new=False):
        self = cls(
            stream=stream,
            name=getattr(stream, 'name', name),
            is_new=is_new
        )
        if is_new:
            self.write_header()
        else:
            self.read_header()
        return self

    def read_memo(self, block_num):
        raise NotImplementedError

    def write_memo(self, memo):
        old_block_count = memo.block_count or 0
        self._get_memo_block_count(memo, also_set=True)

        if memo.block_start is not None and old_block_count <= memo.block_count:
            block_count_changed = False

        else:
            memo.block_start = self.block_count
            block_count_changed = True

        self.stream.seek(memo.block_start * self.block_size)
        self._make_actual_write(memo)

        if block_count_changed:
            self.block_count += memo.block_count
            self.stream.seek(0)
            self.stream.write(struct.pack('>L', self.block_count))

        return memo.block_start

    def _get_memo_block_count(self, memo, also_set=False):
        data_length = len(memo) + self.extra_bytes
        memo_block_count, last_block_size = divmod(data_length, self.block_size)
        if last_block_size:
            memo_block_count += 1

        if also_set:
            memo.block_count = memo_block_count

        return memo.block_count

    def _make_actual_write(self, memo):
        pass

    def _pad(self, memo):
        data_length = len(memo) + self.extra_bytes
        last_block_size = data_length % self.block_size

        padding_size = self.block_size - last_block_size
        if padding_size:
            self.stream.write(b'\0' * padding_size)

    def vacuum(self):
        from udbf.field import MemoField

        memo_fields = [
            (field_name, field)
            for field_name, field in six.iteritems(self._dbf.headerfields)
            if isinstance(field, MemoField)
        ]
        if not memo_fields:
            return
        self._dbf.flush()

        memos = []
        for record in self._dbf:
            for field_name, field in memo_fields:
                memo = record[field_name]
                memos.append(
                    (memo.block_start, memo, record, field_name)
                )
        memos.sort()

        current_block_start = 1
        for block_start, memo, record, field_name in memos:
            if block_start == current_block_start:
                continue
            memo.block_start = current_block_start
            self._make_actual_write(memo)

            current_block_start += memo.block_count

        self.block_count = current_block_start
        self.stream.truncate(self.block_count * self.block_size)


@attrs()
class DBase3MemoFile(MemoFile):
    EOT = b'\x1a\x1a'
    extra_bytes = len(EOT)

    def read_header(self):
        super(DBase3MemoFile, self).read_header()
        self.block_size = 512

    # noinspection PyArgumentList
    def read_memo(self, block_num):
        self.stream.seek(self.block_size * block_num)
        block_count = 0
        value = b''
        while self.EOT not in value:
            data = self.stream.read(self.block_size)
            if not data:
                raise RuntimeError('Unexpected end of file')
            value += data
            block_count += 1
        value = value[:value.find(self.EOT)]
        return Memo(value, MemoType.MEMO, block_num, block_count)

    def _make_actual_write(self, memo):
        self.stream.write(memo.data + self.EOT)
        self._pad(memo)


@attrs()
class DBase4MemoFile(MemoFile):
    memo_header_struct = struct.Struct('<4xL')
    extra_bytes = 8

    def read_header(self):
        super(DBase4MemoFile, self).read_header()
        self.block_size = 512

    # noinspection PyArgumentList
    def read_memo(self, block_num):
        self.stream.seek(self.block_size * block_num)
        data = self.stream.read(8)
        data_size, = self.memo_header_struct.unpack(data)

        value = self.stream.read(data_size)

        memo = Memo(value, MemoType.MEMO, block_num, 0)
        self._get_memo_block_count(memo, also_set=True)
        return memo

    def _make_actual_write(self, memo):
        self.stream.write(self.memo_header_struct.pack(len(memo)))
        if memo.data is not None:
            self.stream.write(memo.data)

        self._pad(memo)


@attrs()
class FoxProMemoFile(MemoFile):
    memo_header_struct = struct.Struct('>LL')
    extra_bytes = memo_header_struct.size

    # noinspection PyArgumentList
    def read_memo(self, block_num):
        self.stream.seek(self.block_size * block_num)
        data = self.stream.read(8)
        if len(data) < 8:
            raise RuntimeError('Unexpected end of file')
        memo_type, length = self.memo_header_struct.unpack(data)
        if memo_type == MemoType.NULL:
            return Memo(b'', memo_type, block_num, 1)
        else:
            data = self.stream.read(length)
            block_count = int(math.ceil((length + self.extra_bytes) / self.block_size))
            return Memo(data, memo_type, block_num, block_count)

    def _make_actual_write(self, memo):
        self.stream.write(self.memo_header_struct.pack(memo.type, len(memo)))
        if memo.data is not None:
            self.stream.write(memo.data)

        self._pad(memo)


@unique
class MemoType(Enum):
    PICTURE = 0
    MEMO = 1
    OBJECT = 2
    NULL = 160


@attrs(slots=True)
class Memo(object):
    data = attrib(validator=validators.instance_of(six.binary_type))
    type = attrib(validator=validators.instance_of((MemoType, int)))  # ???
    block_start = attrib(default=None)
    block_count = attrib(default=None)

    def __len__(self):
        if self.type == MemoType.NULL or self.data is None:
            return 0
        return len(self.data)
