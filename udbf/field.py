import datetime
import re
import struct
import uuid

import six
from attr import attrs, attrib, Factory, validators

from udbf.memo import MemoType, Memo


@attrs()
class FieldRegistry(object):
    registry = attrib(default=Factory(dict))

    def register(self, cls):
        if issubclass(cls, FieldBase) and hasattr(cls, 'code'):
            self.registry[cls.code] = cls
        return cls

    def get(self, code):
        if isinstance(code, six.integer_types):
            code = struct.pack('<B', code)
        return self.registry[code]


field_registry = FieldRegistry()


# noinspection PyUnresolvedReferences
class CharacterCoercesMixin(object):
    def _coerce_to_valid_py_type(self, py_value):
        if py_value is None and self.is_nullable:
            return None

        elif self.is_binary:
            if isinstance(py_value, six.binary_type):
                return py_value
            elif isinstance(py_value, six.text_type):
                return py_value.encode(self._charset or 'utf8')
            return six.binary_type(py_value)

        else:
            if isinstance(py_value, six.text_type):
                return py_value
            if isinstance(py_value, six.binary_type):
                try:
                    return py_value.decode(self._charset or 'utf8')
                except UnicodeError:
                    return py_value
            return six.text_type(py_value)

    def _coerce_to_binary(self, py_value):
        if isinstance(py_value, six.binary_type):
            return py_value
        elif py_value is None:
            return b''
        elif isinstance(py_value, six.text_type):
            return py_value.encode(self._charset or 'utf8')
        return six.binary_type(py_value)


@attrs(slots=True)
class FieldBase(object):
    code = None
    name = attrib(validator=validators.instance_of(six.text_type))
    offset = attrib(validator=validators.instance_of(int))
    length = attrib(validator=validators.instance_of(int))
    decimal_places = attrib(validator=validators.instance_of(int))
    is_system = attrib(default=False, validator=validators.instance_of(bool))
    is_nullable = attrib(default=True, validator=validators.instance_of(bool))
    is_binary = attrib(default=False, validator=validators.instance_of(bool))
    is_auto_increment = attrib(default=Factory, validator=validators.instance_of(bool))
    auto_inc_next = attrib(default=1, validator=validators.instance_of(six.integer_types))
    auto_inc_step = attrib(default=1, validator=validators.instance_of(six.integer_types))
    _dbf = attrib(default=None, type='DBF')
    struct_template = None

    _charset = property(lambda self: self._dbf.charset if self._dbf else None)

    structure = struct.Struct(b'<11sBIBBBIB8x')

    @classmethod
    def from_structure(cls, data, dbf):
        if not len(data) == 32:
            raise ValueError('data must be 32 bytes long')
        name, typecode, offset, length, decimal_places, flags, auto_inc_next, auto_inc_step = cls.structure.unpack(data)
        name = name.rstrip(b'\0 ').decode('ascii')
        is_system = bool(flags & 0x01)
        is_nullable = bool(flags & 0x02)
        is_binary = bool(flags & 0x04)
        is_auto_increment = bool(flags & 0x0c)
        klass = field_registry.get(typecode)
        field = klass(
            name,
            offset, length, decimal_places,
            is_system,
            is_nullable,
            is_binary,
            is_auto_increment,
            auto_inc_next,
            auto_inc_step,
            dbf=dbf,
        )
        return field

    def to_structure(self):
        return self.structure.pack(
            six.binary_type(self.name[:11]).ljust(b'\0'),
            self.code,
            self.offset,
            self.length,
            self.decimal_places,
            int(self.is_system) | int(self.is_nullable) << 1 | int(self.is_binary and 0x0c),
            self.auto_inc_next,
            self.auto_inc_step,
        )

    def from_py(self, py_value, old_value):
        return py_value

    def to_py(self, value):
        return value

    def from_db(self, db_value):
        return db_value

    def to_db(self, value):
        return value


@field_registry.register
@attrs(slots=True)
class CharField(FieldBase, CharacterCoercesMixin):
    code = b'C'

    @property
    def struct_template(self):
        return '{0}s'.format(self.length)

    def from_py(self, py_value, old_value):
        return self._coerce_to_valid_py_type(py_value)

    def from_db(self, db_value):
        """
        :type db_value: bytes|str
        :param db_value:
        :return:
        """
        if self.is_binary:
            return db_value
        else:
            try:
                return db_value.rstrip(b'\0 ').decode(self._charset or 'utf8')
            except UnicodeError:
                return db_value

    def to_db(self, value):
        if isinstance(value, six.binary_type):
            return value
        elif isinstance(value, six.text_type):
            return value.encode(self._charset or 'utf8').ljust(self.length, b'\0')
        else:
            return six.binary_type(value).ljust(self.length, b'\0')


@field_registry.register
@attrs(slots=True)
class CurrencyField(FieldBase):
    code = b'Y'

    struct_template = 'q'

    def from_py(self, py_value, old_value):
        return float(py_value)

    def from_db(self, db_value):
        return db_value / 10000.0

    def to_db(self, value):
        return round(value * 10000)


@field_registry.register
@attrs(slots=True)
class NumericField(FieldBase):
    code = b'N'

    _re_int_value = re.compile(six.b(r'^\d+$'))
    _re_float_value = re.compile(six.b(r'^\.\d+|\d+(\.\d+)?|\d+$'))

    @property
    def struct_template(self):
        return '{0}s'.format(self.length)

    def from_py(self, py_value, old_value):
        if self.decimal_places == 0:
            return int(py_value)
        else:
            return float(py_value)

    def from_db(self, db_value):
        db_value = db_value.strip(b'\0 ')
        db_value_is_null = not db_value
        if db_value_is_null:
            if self.is_nullable:
                return None
            db_value = b'0'
        db_value_is_int = self._re_int_value.match(db_value)
        db_value_is_float = self._re_float_value.match(db_value)
        is_integer = self.decimal_places == 0
        if is_integer:
            if db_value_is_int:
                return int(db_value)
            elif db_value_is_float:
                return round(float(db_value))
        else:
            if db_value_is_int or db_value_is_float:
                return float(db_value)
        raise ValueError(db_value)

    def to_db(self, value):
        rv = b'%*.*f' % (self.length, self.decimal_places, value)
        if len(rv) > self.length:
            point_pos = rv.find(b'.')
            if 0 <= point_pos <= self.length:
                rv = rv[:self.length]
            else:
                raise ValueError('[{0} Numeric overflow'.format(self.name))
        return rv


@field_registry.register
@attrs(slots=True)
class FloatField(NumericField):
    code = b'F'

    def from_py(self, py_value, old_value):
        return float(py_value)

    def from_db(self, db_value):
        rv = super(FloatField, self).from_db(db_value)
        return float(rv)


@field_registry.register
@attrs(slots=True)
class IntegerField(FieldBase):
    code = b'I'

    struct_template = 'i'

    def from_py(self, py_value, old_value):
        return int(py_value)

    def to_db(self, value):
        return int(value)


@field_registry.register
@attrs(slots=True)
class DoubleField(FieldBase):
    code = b'B'

    struct_template = 'd'

    def from_py(self, py_value, old_value):
        return float(py_value)

    def to_db(self, value):
        return float(value)


@field_registry.register
@attrs(slots=True)
class DateField(FieldBase):
    code = b'D'

    struct_template = '8s'

    def from_db(self, db_value):
        db_value = db_value.strip(b' \x00')
        if not db_value:
            return None
        return datetime.datetime.strptime(db_value.decode(), '%Y%m%d').date()

    def to_db(self, value):
        if value is None:
            return ' ' * self.length
        return value.strftime('%Y%m%d').encode()


@field_registry.register
@attrs(slots=True)
class DateTimeField(FieldBase):
    code = b'T'

    struct_template = '8s'

    _struct = struct.Struct('<II')
    JDN_GDN_DIFF = 1721425

    def from_db(self, db_value):
        jdn, msecs = self._struct.unpack(db_value)
        if jdn >= 1:
            return datetime.datetime.fromordinal(jdn - self.JDN_GDN_DIFF) + datetime.timedelta(milliseconds=msecs)

    def to_db(self, value):
        """
        :type value: datetime.datetime
        :param value:
        :return:
        """
        if value is None:
            return self._struct.pack(0, 0)
        assert value, datetime.datetime
        return self._struct.pack(
            value.toordinal() + self.JDN_GDN_DIFF,
            (value.hour * 3600 + value.minute * 60 + value.second) * 1000
        )


@field_registry.register
@attrs(slots=True)
class LogicalField(FieldBase):
    code = b'L'
    struct_template = 'c'

    def from_db(self, db_value):
        if db_value == b'?':
            return -1
        elif db_value in b'NnFf ':
            return False
        elif db_value in b'YyTt':
            return True
        raise ValueError('[{0}] Invalid logical value {1!r}'.format(self.name, db_value))

    def to_db(self, value):
        if value == -1:
            return b'?'
        elif value:
            return b'T'
        else:
            return b'F'


@field_registry.register
@attrs(slots=True)
class UUIDField(FieldBase):
    code = b'U'
    struct_template = '16s'

    def from_db(self, db_value):
        return uuid.UUID(bytes=db_value)

    def to_db(self, value):
        if isinstance(value, uuid.UUID):
            return value.bytes
        elif isinstance(value, six.binary_type) and len(value) == 16:
            return value
        elif isinstance(value, six.text_type):
            return uuid.UUID(hex=value).bytes
        raise ValueError('[{0}] Invalid UUID value {1!r}'.format(self.name, value))


@field_registry.register
@attrs(slots=True)
class MemoField(FieldBase, CharacterCoercesMixin):
    code = b'M'
    memo_type = MemoType.MEMO

    @property
    def struct_template(self):
        if self.length == 4:  # Normal MEMO
            return 'I'
        return '{0}s'.format(self.length)

    # noinspection PyArgumentList
    def from_py(self, py_value, old_value):
        if py_value is None:
            memo_type = MemoType.NULL
        else:
            memo_type = self.memo_type

        binary_value = self._coerce_to_binary(py_value)

        if old_value is None:
            return Memo(binary_value, memo_type)
        else:
            old_value.data = binary_value

    def to_py(self, value):
        if not isinstance(value, Memo):
            raise ValueError('[{0}] Invalid Memo value {1!r}'.format(self.name, value))
        py_value = value.data
        if not self.is_binary:
            py_value = py_value.strip(b'\0 ')
        return self._coerce_to_valid_py_type(py_value)

    def from_db(self, db_value):
        if isinstance(db_value, six.binary_type):
            db_value = db_value.strip(b' \0')
            if not db_value:
                # noinspection PyArgumentList
                return Memo(b'', MemoType.NULL)
            db_value = int(db_value)
        return self._dbf.memo_file.read_memo(db_value)

    # noinspection PyArgumentList
    def to_db(self, value):
        if not isinstance(value, Memo):
            raise ValueError('[{0}] Invalid Memo value {1!r}'.format(self.name, value))

        self._dbf.memo_file.write_memo(value)
        return value.block_start


@field_registry.register
@attrs(slots=True)
class PictureField(MemoField):
    code = b'P'
    memo_type = MemoType.PICTURE


@field_registry.register
@attrs(slots=True)
class GeneralField(MemoField):
    code = b'G'
    memo_type = MemoType.OBJECT


@field_registry.register
@attrs(slots=True)
class FlagsField(FieldBase):
    code = b'0'

    struct_template = 'B'
