#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

from setuptools import setup

__author__ = 'Mikhael "viruzzz-kun" Malkov'
__version__ = '0.1.1-dev'


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

if __name__ == '__main__':
    setup(
        name="udbf",
        version=__version__,
        description="uDBF - DBF files manipulation library",
        long_description=read('README.md'),
        author=__author__,
        author_email="viruzzz.soft@gmail.com",
        license='MIT',
        url="https://bitbucket.org/viruzzz-kun/udbf",
        packages=["udbf"],
        zip_safe=False,
        install_requires=[
            'attrs',
            # 'cattrs',
            'six',
        ],
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Developers",
            "License :: OSI Approved :: MIT License",
            "Programming Language :: Python",
            "Programming Language :: Python :: 2.7",
            "Programming Language :: Python :: 3.3",
            "Programming Language :: Python :: 3.4",
            "Programming Language :: Python :: 3.5",
            "Programming Language :: Python :: 3.6",
            "Programming Language :: Python :: 3.7",
            "Topic :: Database",
            "Topic :: Software Development :: Libraries :: Python Modules",
        ])
